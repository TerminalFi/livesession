# Live Session Server

## Where LiveSession Began
> The idea for LiveSession came about after seeing VS Codes LiveShare capabilities. Seeing as LiveShare was around, I often found myself switching over to VS Code just to use this one feature. I would have a LiveShare session and then once done, switch back to Sublime. Not satisfied with having to switch back and fourth just for this one feature I started to look into this feature set in Sublime Text. I first found [Remote Collab for Sublime Text][remote-collab], however it was very incomplete, required you to run a server and expose your ports, as well as not being updated for 6 years. Some more digging landed me at [Floobits][], which is a more complete implementation, but costs money, and if you don't want to pay then all your coding sessions are _PUBLIC_ which, unless you only work on open source projects is kind of nerve wrecking. Additionally, it was also lacking features. 
>
> With the comes LiveSession! With ST working on the latest iteration of their product, they introduced a ton of new _API_ endpoints that really bring new capabilities to light. One of those capabilities is real time text changes. With this new API, developers are able to see buffer changes incrementally! With that, LiveSession was born! The backend is being written in Go.. no specific reason and is designed to be horizontally scalable. Quick peek tech-stack (LoadBalancer → X Servers → Redis) this allows us to implement multi-servers, have clients loadbalancer between them automatically, and Redis ties communication together on a per session basis! The plugin is written in python 3.8+ and utilizes Sublime Text's API and plugin hosts.

** Loadbalancer is not currently implemented
** Multiservers is not currently implemented


## Status: v2.0.0

## Description

This is the server that drives the Sublime Text © Live Session plugin. It utilizes websockets to stream changes to and from the clients attached to individual sessions. It's very Alpha right now.

## Notes

🚨 Encryption: Over TLS & Body AES encryption is enabled




[remote-collab]: https://packagecontrol.io/packages/RemoteCollab