package node

import (
	"time"

	uuid "github.com/satori/go.uuid"

	j "encoding/json"
)

type Payload struct {
	Timestamp time.Time    `json:"timestamp"`
	UID       string       `json:"uid"`
	Type      int          `json:"type"`
	Session   *Session     `json:"session,omitempty"`
	Sender    *Client      `json:"sender,omitempty"`
	Body      j.RawMessage `json:"body,omitempty"`
}

func NewPayload(kind int) *Payload {
	return &Payload{
		Timestamp: time.Now(),
		UID:       uuid.NewV4().String(),
		Type:      kind,
	}
}