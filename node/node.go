/*
Package node contains all the brains behind the LiveSession Server
*/
package node

import (
	"runtime"
	"time"

	"github.com/apex/log"
	jsoniter "github.com/json-iterator/go"
	"gitlab.com/theseceng/livesession/metrics"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

const (
	serverRestartReason     = "server_restart"
	remoteDisconnectReason  = "remote"
	statsCollectInterval    = 5 * time.Second
	metricsGoroutines       = "goroutines_num"
	metricsMemSys           = "mem_sys_bytes"
	metricsClientsNum       = "clients_num"
	metricsUniqClientsNum   = "clients_uniq_num"
	metricsSessionsNum      = "broadcast_sessions_num"
	metricsFailedAuths      = "failed_auths_total"
	metricsReceivedMsg      = "client_msg_total"
	metricsUnknownReceived  = "failed_client_msg_total"
	metricsBroadcastMsg     = "broadcast_msg_total"
	metricsUnknownBroadcast = "failed_broadcast_msg_total"
)

type disconnectMessage struct {
	Type      string `json:"type"`
	Reason    string `json:"reason"`
	Reconnect bool   `json:"reconnect"`
}

func (d *disconnectMessage) toJSON() []byte {
	jsonStr, err := json.Marshal(&d)
	if err != nil {
		panic("Failed to build disconnect JSON 😲")
	}
	return jsonStr
}

// AppNode describes a basic node interface
type AppNode interface {
	HandlePubSub(raw []byte)
}

// Node represents the whole application
type Node struct {
	Metrics *metrics.Metrics

	hub        *Hub
	shutdownCh chan struct{}
	log        *log.Entry
}

// NewNode builds new node struct
func NewNode(metrics *metrics.Metrics) *Node {
	node := &Node{
		Metrics:    metrics,
		shutdownCh: make(chan struct{}),
		log:        log.WithFields(log.Fields{"context": "node"}),
	}

	node.hub = NewHub()
	node.registerMetrics()

	return node
}

// Start runs all the required goroutines
func (n *Node) Start() {
	go n.hub.Run()
	go n.collectStats()
}

// HandlePubSub parses incoming pubsub message and broadcast it
func (n *Node) HandlePubSub(raw []byte) {
	payload := &Payload{}
	err := json.Unmarshal(raw, payload)
	if err != nil {
		n.Metrics.Counter(metricsUnknownBroadcast).Inc()
		n.log.Warnf("Failed to parse pubsub message '%s' with error: %v", raw, err)
		return
	}
	n.Broadcast(payload)
}

// Shutdown stops all services (hub, controller)
func (n *Node) Shutdown() {
	close(n.shutdownCh)

	if n.hub != nil {
		n.hub.Shutdown()

		active := len(n.hub.sessions)

		if active > 0 {
			n.log.Infof("Closing active connections: %d", active)
			payload := &Payload{Type: TypeExit}
			for _, session := range n.hub.sessions {
				for _, client := range session.Clients {
					client.Send(payload)
					client.Disconnect()
				}
			}
			n.log.Info("All active connections closed")
		}
	}
}

// Authenticate authenticates a client with a session
func (n *Node) Authenticate(c *Client, sessionUID string, passphrase string) error {
	n.hub.RLock()
	s, ok := n.hub.sessions[sessionUID]
	if !ok {
		n.hub.RUnlock()
		return ErrInvalidSessionID
	}
	if s.Passphrase != passphrase {
		n.hub.RUnlock()
		return ErrInvalidSessionPassphrase
	}
	n.hub.RUnlock()

	s.Lock()
	s.Clients[c.UID] = c
	s.Unlock()
	return nil
}

// Broadcast message to session
func (n *Node) Broadcast(payload *Payload) {
	n.Metrics.Counter(metricsBroadcastMsg).Inc()
	n.hub.broadcast <- payload
}

func (n *Node) collectStats() {
	for {
		select {
		case <-n.shutdownCh:
			return
		case <-time.After(statsCollectInterval):
			n.collectStatsOnce()
		}
	}
}

func (n *Node) collectStatsOnce() {
	n.Metrics.Gauge(metricsGoroutines).Set(runtime.NumGoroutine())

	var m runtime.MemStats
	runtime.ReadMemStats(&m)
	n.Metrics.Gauge(metricsMemSys).Set64(int64(m.Sys))

	n.Metrics.Gauge(metricsClientsNum).Set(n.hub.Size())
	n.Metrics.Gauge(metricsSessionsNum).Set(n.hub.SessionSize())
}

func (n *Node) registerMetrics() {
	n.Metrics.RegisterGauge(metricsGoroutines, "The number of go routines")
	n.Metrics.RegisterGauge(metricsMemSys, "The total bytes of memory obtained from the OS")
	n.Metrics.RegisterGauge(metricsClientsNum, "The number of active clients")
	n.Metrics.RegisterGauge(metricsSessionsNum, "The number of active sessions")
	n.Metrics.RegisterCounter(metricsFailedAuths, "The total number of failed authentication attempts")
	n.Metrics.RegisterCounter(metricsReceivedMsg, "The total number of received messages from clients")
	n.Metrics.RegisterCounter(metricsUnknownReceived, "The total number of unrecognized messages received from clients")
	n.Metrics.RegisterCounter(metricsBroadcastMsg, "The total number of messages received through PubSub (for broadcast)")
	n.Metrics.RegisterCounter(metricsUnknownBroadcast, "The total number of unrecognized messages received through PubSub")
}
