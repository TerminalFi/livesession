package util

import (
	"github.com/segmentio/fasthash/fnv1a"
)

func Hash(value string) uint64 {
	return fnv1a.HashString64(value)
}
