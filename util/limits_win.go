// +build windows

package util

// OpenFileLimit is not supported on Windows
func OpenFileLimit() string {
	return "unsupported"
}
