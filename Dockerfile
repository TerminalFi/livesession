# intentionally not multistage..
FROM golang:1.14.4-alpine

LABEL org.opencontainers.image.authors="ls@ls.sublimetext.io" \
    org.opencontainers.image.vendor="LiveSession" \
    org.opencontainers.image.title="LiveSession" \
    org.opencontainers.image.description="LiveSession Server" \
    org.opencontainers.image.source=""

# Install basics
RUN apk --update --no-cache add build-base mercurial bash gawk sed grep git gcc make libpcap-dev libpcap linux-headers musl-dev

# Install some package dependencies
RUN go get github.com/cespare/reflex
ENV CGO_ENABLED=1
ADD . /app
WORKDIR /app
RUN go build -installsuffix 'static' -o lss
VOLUME [ "/app" ]

CMD [ "./lss", "serve", "-c", "./certs/cert1.pem", "-k", "./certs/privkey1.pem" ]